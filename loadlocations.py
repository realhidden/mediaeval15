import sys, csv, numpy, tfidf, operator, re
from sklearn.cluster import SpectralClustering
from xml.dom import minidom

# forras:http://www.ranks.nl/stopwords
stoplist = []
with open("stopwords.txt") as f:
    stoplist = f.readlines()
stoplist = list(map(lambda s: s.strip(), stoplist))


def trimList(l):
    l = l.lower();

    # html crap
    l = l.replace('&amp;', "&");
    l = l.replace('&amp;', "&");
    l = l.replace('&amp;', "&");
    l = l.replace('&quot;', "'");
    l = l.replace('&apos;', "'");
    l = l.replace('&lt;', '<');
    l = l.replace('&gt;', '>');
    l = re.sub('<.*?>', '', l)

    # points and stuff
    for i in ['.', ',', '-', ':', ';', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '(', ')', '_', '@']:
        l = l.replace(i, '');

    # split to words
    l = l.split(' ');

    # stopwords
    l = list(filter(lambda a: a not in stoplist, l))

    return l


def createLine(run_id, qid, docno, rank):
    return qid + " 0 " + docno + " " + str(rank) + " " + "%.2f" % (1 - rank / 50) + " " + run_id


def getHOG(basedir, name, photoid):
    with open(basedir + 'descvis/img/' + name + " HOG.csv", 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            if (row[0] == str(photoid)):
                return row


def getCN(basedir, name, photoid):
    with open(basedir + 'descvis/img/' + name + " CN.csv", 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            if (row[0] == str(photoid)):
                ret = []
                for i in range(1, len(row)):
                    ret.append(float(row[i]))
                return ret


def getCM(basedir, name, photoid):
    with open(basedir + 'descvis/img/' + name + " CM.csv", 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            if (row[0] == str(photoid)):
                ret = []
                for i in range(1, len(row)):
                    ret.append(float(row[i]))
                return ret


def getFACE(basedir, name, photoid):
    with open(basedir + 'descvis/img/' + name + " FACE.csv", 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            if (row[0] == str(photoid)):
                ret = []
                for i in range(1, len(row)):
                    ret.append(float(row[i]))
                return ret


def diffCM(a, b):
    diff = 0
    for i in range(1, len(a)):
        weight = 1
        if (i >= 3):
            weight = 0.3
        if (i >= 6):
            weight = 0.1
        diff += abs(a[i] - b[i]) * weight
    return diff


def diffCN(a, b):
    diff = 0
    for i in range(1, len(a)):
        diff += abs(a[i] - b[i])
    return diff


def getDistanceMX(items):
    mx = numpy.zeros(shape=(len(items), len(items)))
    for i in range(len(items)):
        for j in range(i, len(items)):
            if (j < i):
                continue
            dis = 0
            if (i != j):
                dis += diffCM(items[i]['cm'], items[j]['cm']) * 5
                dis += diffCN(items[i]['cn'], items[j]['cn'])
            mx[i][j] = dis
            mx[j][i] = dis

    return mx


def getSimilarity(text1, text2, tf, allpicture):
    dist = 0

    for t1 in text1:
        # the other has the same word, no change
        if t1 in text2:
            continue

        tf1tf = tf[t1]

        if tf1tf < 5:
            dist += 2
        else:
            if tf1tf > allpicture / 5:
                dist += 0.1
            else:
                dist += tf1tf / allpicture
    return dist


def getTextDistanceMX(items, thetf, allphoto):
    mx = numpy.zeros(shape=(len(items), len(items)))
    for i in range(len(items)):
        for j in range(i, len(items)):
            if (j < i):
                continue
            dis = 0
            if (i != j):
                dis = getSimilarity(items[i]['title'], items[j]['title'], thetf, allphoto)
                dis += getSimilarity(items[j]['title'], items[i]['title'], thetf, allphoto)

                dis += 2 * getSimilarity(items[j]['tags'], items[i]['tags'], thetf, allphoto)
                dis += 2 * getSimilarity(items[i]['tags'], items[j]['tags'], thetf, allphoto)

                dis += 0.5 * getSimilarity(items[j]['desc'], items[i]['desc'], thetf, allphoto)
                dis += 0.5 * getSimilarity(items[i]['desc'], items[j]['desc'], thetf, allphoto)

            dis=abs(dis/2)+0.0001*(i*j)
            mx[i][j] = dis
            mx[j][i] = dis

    return mx


def runOne(basedir, runid, num, name, usevisual, usetext):
    locfile = basedir + 'xml/' + name + '.xml'
    xmldoc = minidom.parse(locfile)
    photos = xmldoc.getElementsByTagName('photo')

    tf = tfidf.tfidf();

    # data from the first filter
    filter_1 = []

    # other stuff, needs to go to the END of the list
    filter_end = []

    origrank = 0
    for item in photos:
        photoid = item.getAttribute("id")
        origrank += 1

        # my photo representation
        myphoto = {'id': photoid, 'originalrank': origrank}

        if usevisual:
            # get CN
            cn = getCN(basedir, name, photoid)
            # if black > 10%
            if (float(cn[0]) > 0.8):
                filter_end.append(myphoto)
                continue

            # get FACE
            face = getFACE(basedir, name, photoid)
            # if face proportation > 0%
            if (face[0] > 0.0):
                filter_end.append(myphoto)
                continue

            # grab extra data
            myphoto['cn'] = cn
            myphoto['cm'] = getCM(basedir, name, photoid)
            #myphoto['hog'] = getHOG(basedir, name, photoid)

        if usetext:
            myphoto['title'] = trimList(item.getAttribute('title'))
            myphoto['tags'] = trimList(item.getAttribute('tags'))
            myphoto['desc'] = trimList(item.getAttribute('description'))

            tf.addDocument(photoid + "title", myphoto['title']);
            tf.addDocument(photoid + "tags", myphoto['tags']);
            tf.addDocument(photoid + "desc", myphoto['desc']);

        # add to pool
        filter_1.append(myphoto)

    # final result
    finalresult = []

    # filter for first 150
    while len(filter_1) > 150:
        pop1 = filter_1.pop()
        filter_end.insert(0, pop1)

    # SpectralClustering
    if len(filter_1) > 10:
        dMX = numpy.zeros(shape=(len(filter_1), len(filter_1)))

        if usetext:
            sorted_corpus = sorted(tf.corpus_dict.items(), key=operator.itemgetter(1))
            thetf = {}
            for key in sorted_corpus:
                thetf[key[0]] = key[1]
            dtextMX = getTextDistanceMX(filter_1, thetf, len(photos))
            for i in range(len(filter_1)):
                for j in range(len(filter_1)):
                    dMX[i][j] += dtextMX[i][j]

        if usevisual:
            dvisMX = getDistanceMX(filter_1)
            for i in range(len(filter_1)):
                for j in range(len(filter_1)):
                    dMX[i][j] += dvisMX[i][j]

        db = SpectralClustering(affinity="precomputed", n_clusters=min(10, len(filter_1)), random_state=None).fit(dMX)

        # grab labels
        labels = db.labels_
        maxlabels = max(db.labels_) + 1

        # throw into clusters
        csorter = [[] for x in range(maxlabels)]
        for i in range(0, len(filter_1)):
            csorter[labels[i]].append(i)

        # use everything
        while True:
            fline = []
            for i in range(0, maxlabels):
                if len(csorter[i]) == 0:
                    continue
                pop1 = csorter[i][0]
                del csorter[i][0]
                fline.append(pop1)

            if (len(fline) == 0):
                break
            fline = sorted(fline)
            for f1 in fline:
                finalresult.append(filter_1[f1]['id'])
        else:
            for f2 in filter_1:
                finalresult.append(f2['id'])

    # some crap append
    for f2 in filter_end:
        finalresult.append(f2['id'])

    # print result
    for i in range(50):
        print(createLine(runid, num, finalresult[i], i))


# print(str(sys.argv))

basedir = 'dataset/testset/multi-topic/'
runid = 'test'
topicfile = basedir + 'testset_multi-topics.xml'

usevis = ('usevis' in sys.argv) or ('usevistext' in sys.argv)
usetext = ('usetext' in sys.argv) or ('usevistext' in sys.argv)

xmldoc = minidom.parse(topicfile)
topics = xmldoc.getElementsByTagName('topic')
for item in topics:
    numberid = item.getElementsByTagName('number')[0].firstChild.data
    title = item.getElementsByTagName('title')[0].firstChild.data
    if (numberid in sys.argv) or (len(sys.argv) == 2):
        runOne(basedir, runid, numberid, title, usevis, usetext)
    sys.stdout.flush()
