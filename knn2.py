import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn import neighbors, datasets
from sklearn.cluster import DBSCAN
from sklearn.metrics.pairwise import euclidean_distances

n_neighbors = 2


def mydist(x, y):
    if ((len(x)!=2) or (len(y)!=2)):
        return 0

    print([x,'vs',y])
    return np.sum((x - y) ** 2)


# import some data to play with
items = np.array([
    [10, 10],
    [10, 13],
    [5, 20],
    [10, 13],
    [5, 20],
    [10, 13],
    [5, 20]
])

D = euclidean_distances(items,items)
print(D)
db = DBSCAN(metric="precomputed",eps=0.3, min_samples=2).fit(D)

print(db.labels_)

#nbrs = neighbors.NearestNeighbors(n_neighbors=n_neighbors, algorithm='ball_tree', metric='pyfunc', func=mydist)
#nbrs.fit(items)
#A = nbrs.kneighbors(items)
#print(A)
