import glob, os
import tfidf
import operator
import re
from xml.dom import minidom

# forras:http://www.ranks.nl/stopwords
stoplist = []
with open("stopwords.txt") as f:
    stoplist = f.readlines()
stoplist = list(map(lambda s: s.strip(), stoplist))


def trimList(l):
    l = l.lower();

    # html crap
    l = l.replace('&amp;', "&");
    l = l.replace('&amp;', "&");
    l = l.replace('&amp;', "&");
    l = l.replace('&quot;', "'");
    l = l.replace('&apos;', "'");
    l = l.replace('&lt;', '<');
    l = l.replace('&gt;', '>');
    l = re.sub('<.*?>', '', l)

    # points and stuff
    for i in ['.', ',', '-', ':', ';', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '(', ')', '_','@']:
        l = l.replace(i, '');

    # split to words
    l = l.split(' ');

    # stopwords
    l = list(filter(lambda a: a not in stoplist, l))

    return l


def getSimilarity(text1, text2, tf, allpicture):
    dist = 0

    for t1 in text1:
        # the other has the same word, no change
        if t1 in text2:
            continue

        tf1tf=tf[t1]

        if tf1tf < 5:
            dist += 2
        else:
            if tf1tf > allpicture / 5:
                dist += 0.1
            else:
                dist += tf1tf / allpicture
    return dist


def processone(file):
    xmldoc = minidom.parse(file)
    photos = xmldoc.getElementsByTagName('photo')
    tf = tfidf.tfidf();

    filter_1 = []

    for item in photos:
        photoid = item.getAttribute("id")
        title = trimList(item.getAttribute('title'))
        tags = trimList(item.getAttribute('tags'))
        desc = trimList(item.getAttribute('description'))

        tf.addDocument(photoid + "title", title);
        tf.addDocument(photoid + "tags", tags);
        tf.addDocument(photoid + "desc", desc);

        myphoto = {'id': photoid, 'title': title, 'tags': tags, 'desc': desc}
        filter_1.append(myphoto)

    sorted_corpus = sorted(tf.corpus_dict.items(), key=operator.itemgetter(1))

    # create distance matrix
    # for key in sorted_corpus:
    #    print(key[0] + " - " + str(key[1]))
    thetf={}
    for key in sorted_corpus:
        thetf[key[0]]=key[1]

    #print(filter_1[0], 'vs', filter_1[1])
    for i in range(0,len(filter_1)):
        for j in range(i+1,len(filter_1)):
            dist = getSimilarity(filter_1[i]['title'], filter_1[j]['title'], thetf, len(filter_1))
            dist += getSimilarity(filter_1[j]['title'], filter_1[i]['title'], thetf, len(filter_1))

            dist += 2*getSimilarity(filter_1[j]['tags'], filter_1[i]['tags'], thetf, len(filter_1))
            dist += 2*getSimilarity(filter_1[i]['tags'], filter_1[j]['tags'], thetf, len(filter_1))

            dist += 0.5*getSimilarity(filter_1[j]['desc'], filter_1[i]['desc'], thetf, len(filter_1))
            dist += 0.5*getSimilarity(filter_1[i]['desc'], filter_1[j]['desc'], thetf, len(filter_1))
            print(dist)


basedir = os.getcwd() + "/dataset/devset/xml/"
os.chdir(basedir)
for file in glob.glob("*.xml"):
    print(file)
    processone(basedir + file)
    break
