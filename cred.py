import sys, csv, numpy, tfidf, operator, re
from xml.dom import minidom
import os.path


def createLine(run_id, qid, docno, rank):
    return qid + " 0 " + docno + " " + str(rank) + " " + "%.2f" % (1 - rank / 50) + " " + run_id


def getCred(basedir, userid):
    ret = {}
    ret['faceProportion'] = "0.0"
    ret['locationSimilarity'] = "0.0"

    if os.path.isfile(basedir + 'desccred/' + userid + ".xml.csv") == False:
        return ret
    # xmldoc = minidom.parse(basedir + 'desccred/' + userid + ".xml")
    # cred = xmldoc.getElementsByTagName('credibilityDescriptors')[0]
    # ret['faceProportion'] = cred.getElementsByTagName('faceProportion')[0].firstChild.data
    # ret['locationSimilarity'] = cred.getElementsByTagName('locationSimilarity')[0].firstChild.data
    with open(basedir + 'desccred/' + userid + ".xml.csv", 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            if row[1] == 'Array':
                continue
            if row[1] == 'NULL':
                continue

            if (row[0] == 'faceProportion'):
                ret['faceProportion'] = row[1]
            if (row[0] == 'locationSimilarity'):
                ret['locationSimilarity'] = row[1]

    return ret


def runOne(basedir, runid, num, name):
    locfile = basedir + 'xml/' + name + '.xml'
    xmldoc = minidom.parse(locfile)
    photos = xmldoc.getElementsByTagName('photo')

    # data from the first filter
    filter_1 = []

    # other stuff, needs to go to the END of the list
    filter_end = []

    origrank = 0
    for item in photos:
        photoid = item.getAttribute("id")
        origrank += 1

        userid = item.getAttribute('userid')

        # my photo representation
        myphoto = {'id': photoid, 'originalrank': origrank}

        if origrank > 100:
            filter_end.append(myphoto)
            continue

        cred = getCred(basedir, userid)

        # locsim > 1.3
        if float(cred['locationSimilarity']) > 3.0:
            filter_end.append(myphoto)
            continue
        # too much face
        if float(cred['faceProportion']) > 0.1:
            filter_end.append(myphoto)
            continue

        # add to pool
        filter_1.append(myphoto)

    # final result
    finalresult = []

    # some crap append
    for f1 in filter_1:
        finalresult.append(f1['id'])

    # some crap append
    for f2 in filter_end:
        finalresult.append(f2['id'])

    # print result
    for i in range(50):
        print(createLine(runid, num, finalresult[i], i))


# print(str(sys.argv))

basedir = 'dataset/testset/one-topic/'
runid = 'test'
topicfile = basedir + 'testset_one-topics.xml'

xmldoc = minidom.parse(topicfile)
topics = xmldoc.getElementsByTagName('topic')
for item in topics:
    numberid = item.getElementsByTagName('number')[0].firstChild.data
    title = item.getElementsByTagName('title')[0].firstChild.data
    runOne(basedir, runid, numberid, title)
    sys.stdout.flush()
